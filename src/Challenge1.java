import java.util.Scanner;

public class Challenge1 {
    public static void main(String[] args) {
        String ulg = "y";
        boolean exit;
        while (ulg.equals("y"))// perintah untuk memuat pilihan kembali menu awal

            /*Program untuk menu utama untuk pilihan apa yang akan di hitung*/
        {
            System.out.println(" ");
            Scanner input = new Scanner(System.in);
            String ul="",lagi;
            System.out.println("---------------------------------------");
            System.out.println(" Kalkulator Penghitung Luas Dan Volume ");
            System.out.println("---------------------------------------");
            System.out.println("1. Hitung Luas Bidang");
            System.out.println("2. Hitung Volume");
            System.out.println("3. Keluar");
            System.out.println("---------------------------------------");

            int pilih = input.nextInt();//program menentukan piliihan

            switch (pilih) {//penggunaan program nested switch case untuk menentukan pilihan
                case 1:
                    System.out.println("---------------------------------------");
                    System.out.println("Pilih bidang yang akan dihitung");
                    System.out.println("---------------------------------------");
                    System.out.println("1. Persegi");//menu bangun datar
                    System.out.println("2. Lingkaran");//menu bangun datar
                    System.out.println("3. Segitiga");//menu bangun datar
                    System.out.println("4. Persegi Panjang");//menu bangun datar
                    System.out.println("0. Kembali ke menu sebelumnya");//menu untuk kembali ke menu awal
                    System.out.println("---------------------------------------");
                    Scanner masukan1 = new Scanner(System.in);
                    int submenu1 = input.nextInt();

                    switch(submenu1) {
                        case 1 :
                            Scanner inputan = new Scanner(System.in);
                            double s, luas;
                            System.out.println("\n-->Persegi<--");
                            System.out.print("Masukkan Panjang sisi : ");
                            s = input.nextDouble();
                            System.out.println("=>");
                            luas = s * s;
                            System.out.print("Luas  = " + (int)luas);
                            System.out.println("");
                            break;
                        case 2 :
                            Scanner inputling = new Scanner(System.in);

                            System.out.println("\n-->Lingkaran<--");

                            //Deklarasi variabel
                            double luasling, phi=3.14;
                            int r;

                            //Input nilai jari-jari
                            System.out.print("Masukan Jari-jari : ");
                            r=input.nextInt();

                            //Menghitung luas lingkaran
                            luas=phi*r*r;

                            //Tampilkan hasil
                            System.out.println("Luas Lingkaran = "+luas);
                            break;
                        case 3:
                            Scanner inputseg =new Scanner(System.in);
                            int a, t;
                            double luasseg;

                            System.out.println("\n-->Segitiga<--");
                            System.out.print("Masukan Alas : ");
                            a=input.nextInt();
                            System.out.print("Masukan Tinggi : ");
                            t=input.nextInt();

                            luas=0.5*a*t;
                            System.out.println("Luas Segitiga : "+luas);
                            break;
                        case 4:
                            Scanner inputluaS =new Scanner(System.in);
                            int panjang, lebar, luass;

                            System.out.println("\n-->Persegi Panjang<--");
                            System.out.print("Masukan Panjang : ");
                            panjang=input.nextInt();
                            System.out.print("Masukan Lebar : ");
                            lebar=input.nextInt();

                            luass=panjang*lebar;

                            System.out.println("Luas Persegi Panjang : "+luass);
                            break;
                    }
                    break;

                case 2:
                    System.out.println("---------------------------------------");
                    System.out.println("Pilih bidang yang akan dihitung");
                    System.out.println("---------------------------------------");
                    System.out.println("1. Kubus");
                    System.out.println("2. Balok");
                    System.out.println("3. Tabung");
                    System.out.println("0. Kembali ke menu sebelumnya");
                    System.out.println("---------------------------------------");
                    Scanner masukan2 = new Scanner(System.in);
                    int submenu2 = input.nextInt();

                    switch(submenu2) {
                        case 1 :
                            Scanner inputkub = new Scanner (System.in);

                            System.out.println("\n-->Kubus<--");
                            int sisi;
                            int Volume;
                            System.out.println("Menghitung Volume Kubus");
                            System.out.print("Masukkan sisi : ");
                            sisi = input.nextInt();

                            Volume = sisi*sisi*sisi;

                            System.out.println("Volume Kubus = " + Volume);
                            break;
                        case 2 :
                            Scanner inputlok = new Scanner(System.in);
                            int panjang, lebar, tinggi, hasil;


                            System.out.println("\n-->Balok<--");
                            // logic
                            System.out.print("Masukan Panjang Balok: ");
                            panjang = input.nextInt();
                            System.out.print("Masukan Lebar balok: ");
                            lebar = input.nextInt();
                            System.out.print("Masukan Tinggi Balok: ");
                            tinggi = input.nextInt();

                            // hitung volume balok;
                            hasil = panjang * lebar * tinggi;

                            System.out.println("Volume Balok tersebut adalah: " + hasil);
                            break;
                        case 3:
                            double r, t;
                            final double PHI = 3.14;
                            double luasTabung, volTabung;

                            System.out.println("\n-->Tabung<--");
                            Scanner scanInput = new Scanner(System.in);

                            System.out.print("Jari-jari alas: ");
                            r = scanInput.nextDouble();
                            System.out.print("Tinggi tabung: ");
                            t = scanInput.nextDouble();


                            volTabung = (PHI * r * r) * t;

                            System.out.println("Volume Tabung = " + volTabung);
                            break;
                    }
                    break;



                case 3:
                    System.exit(4);
                    System.out.println("Warning !! Pilihan tidak tersedia !! ");
                    break;

            }
            System.out.println("=====================================");
            System.out.print("Kembali ke menu sebelumnya (y/t)? ");
            ulg = input.next();
        }
    }
}
